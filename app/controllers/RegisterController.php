<?php
namespace App\Controllers;

//require_once '../app/models/User.php';

use \App\Models\User;
/**
*
*/
class RegisterController
{

    function __construct()
    {

    }

    public function index()
    {
        // echo "<p>En Index()</p>";
        require "../app/views/register.php";
    }

     public function register(){
        $jugadores = new Jugadores();

        $jugadores->name = $_REQUEST['name'];
        $jugadores->puesto = $_REQUEST['puesto'];
        $jugadores->birthdate = $_POST['ano'].'-'.$_POST['mes'].'-'.$_POST['dia'];
        $jugadores->insert();
        header('Location:/jugador');
    }
}
