<?php
namespace App\Controllers;

//require_once '../app/models/User.php';

use \App\Models\User;

class JugadorController
{

    function __construct()
    {
        session_start();


    }
    public function index(){
        require "../app/views/jugador.php";

    }

    public function jugador()
    {
        $pagesize = 2;
         $jugadores = Jugadores::paginate($pagesize);
         $rowCount = Jugadores::rowCount();

        if (isset($_REQUEST['page'])){
          $page = (integer)$_REQUEST['page'];
         }else{
            $page = 1;
          }
         $pages = ceil($rowCount / $pagesize);
         $page = $_REQUEST['page'];
        require "../app/views/jugador.php";

         $jugadores = Jugadores::all();

        require "../app/views/jugador.php";

    }

     public function titulares($arguments){
         $jugadores = $_SESSION['jugadores'];
           $jugadores[] = $_REQUEST['jugadores'];
           $_SESSION['jugador'] = $jugadores;

        $id = (int) $arguments[0];
        //var_dump($id);
        $jugador = Jugadores::find($id);
        require "../app/views/titular.php";
    }
    public function register(){


        require "../app/views/register.php";
    }

 public function delete($arguments){
        $id = (int)$arguments[0];
        Jugadores::remove($id);
        header('Location:/jugador');
    }


}
