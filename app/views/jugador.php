<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>
  <h1>Jugadores</h1>
  <table border = "1" class="table table-striped">
  <?php foreach ($jugadores as $jugador): ?>
    <tr>
        <td> <?php echo $jugador ->id  ?></td>
        <td> <?php echo $jugador ->name  ?></td>
        <td> <?php echo $jugador ->puesto  ?></td>
        <td><?php echo $jugador ->birthdate ?></td>
        <td><a href="/jugador/titular/<?php echo $user ->id ?>" > Titular </a></td>
    </tr>

    <?php endforeach ?>


</table>

  <hr>
  <?php for($i=1; $i <= $page; $i++) {?>
            <?php if ($i == $page) {?>

            <?php } ?>
           <a href="/jugador?page=<?php echo $i?>" class="btn btn-primary"> <?php echo $i ?></a>


        <?php  }?>
        <hr>
        <a href="/jugador/register">Nuevo</a>


  <?php require "../app/views/parts/footer.php" ?>


</body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?php require "../app/views/parts/scripts.php" ?>
</html>
