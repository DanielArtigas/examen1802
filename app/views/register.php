<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>


<table border = "1" class="table table-striped">
<div class="starter-template">
<h1>Alta de Jugador</h1>


  <form method="post" action="/jugador">

   <div class="form-group">
    <label>Nombre:</label>
    <input type="text" class="form-control" name="name">
  </div>


   <div class="form-group">
    <label>Puesto:</label>
    <select>
      <option>Portero</option>
      <option>Defensa</option>
      <option>Centrocampista</option>
      <option>Delantero</option>
    </select>
  </div>
  <label>Fecha de Nacimiento:  </label>
  <select name="dia">
        <?php
        for ($i=1; $i<=31; $i++) {
            if ($i == date('j'))
                echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
 </select>

 <select name="mes">
        <?php
        for ($i=1; $i<=12; $i++) {
            if ($i == date('m'))
                echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
</select>

<select name="ano">
        <?php
        for($i=date('o'); $i>=1910; $i--){
            if ($i == date('o'))
                echo '<option value="'.$i.'" selected>'.$i.'</option>';
            else
                echo '<option value="'.$i.'">'.$i.'</option>';
        }
        ?>
</select>

    <button type="submit" class="btn btn-default">Enviar</button>

  </form>
</div>
  <?php require "../app/views/parts/footer.php" ?>


</body>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?php require "../app/views/parts/scripts.php" ?>
</html>
