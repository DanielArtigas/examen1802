<?php
namespace App\Models;

use PDO;
use Core\Model;

//require_once '../core/Model.php';

/**
*
*/
class User extends Model
{

    function __construct()
    {

    }
    public static function all(){

        $db = Jugadores::db();
        $statememt = $db->query('SELECT * FROM jugadores');

        //$statememt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $jugadores = $statememt->fetchAll(PDO::FETCH_CLASS,Jugadores::class);

        return $jugadores;
        }


    public function find($id){

    $db = Jugadores::db();

    $statememt = $db->prepare('SELECT * FROM jugadores WHERE id=:id');
    $statememt->execute(array(':id' => $id));
    $statememt->setFetchMode(PDO::FETCH_CLASS,Jugadores::class);
    $jugador = $statememt->fetch(PDO::FETCH_CLASS);

    return $jugador;


    }



    public function insert()
    {
    $db = Jugador::db();

    $statememt = $db->prepare('INSERT INTO jugadores(name,puesto,birthdate)
        VALUES(:name, :puesto, :birthdate)');

    $statememt->bindValue(':name', $this->name);
    $statememt->bindValue(':puesto', $this->puesto);
    $statememt->bindValue(':birthdate', $this->birthdate);

    return $statememt->execute();

    }




}
